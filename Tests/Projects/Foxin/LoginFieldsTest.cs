﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Projects.Foxin
{
    //in case tests are run in quick succession please note that tests might fail due to server 503 error
    [TestFixture]
    [Parallelizable]
    class LoginFieldsTest
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL("http://foxin.cz/wp-login.php");
        }

        [Test]
        public void loginFieldsValidationTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            Assert.AreEqual(loginPage.GetUsernameLabel(), "Username or Email Address");
            Assert.AreEqual(loginPage.GetPasswordLabel(), "Password");
        }

        [Test]
        public void loginNotRegisteredUSerValidationTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string message = loginPage
                .InputUsername("p")
                .InputPassword("p")
                .ClickOnLoginButtonToValidate()
                .GetMessage();
            Assert.IsTrue(message.Contains("Invalid username."));
        }

        [Test]
        public void loginUpperCaseValidationTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string message = loginPage
                .InputUsername("testuser1")
                .InputPassword("TESTPASSWORD1")
                .ClickOnLoginButtonToValidate()
                .GetMessage();
            Assert.IsTrue(message.Contains("The password you entered for the username testuser1 is incorrect."));
        }

        [Test]
        public void loginWithoutPasswordTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string message = loginPage
                .InputUsername("testuser1")
                .InputPassword("")
                .ClickOnLoginButtonToValidate()
                .GetMessage();
            Assert.IsTrue(message.Contains("The password field is empty."));
        }

        [Test]
        public void loginWithoutUsernameTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string message = loginPage
                .InputUsername("")
                .InputPassword("testpassword1")
                .ClickOnLoginButtonToValidate()
                .GetMessage();
            Assert.IsTrue(message.Contains("The username field is empty."));
        }

        [OneTimeTearDown]
        public void closeBrowser()
        {
            basePage.CloseDriver();
        }
    }
}
