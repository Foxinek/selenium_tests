﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Pages.Reports.SEO
{
    [Serializable]
    class HomePage:BasePage
    {
        public HomePage(IWebDriver driver)
        {
            Search_field = InitElement(driver, ".//div[@class='front__landing-text']//input[@name='url']");
            Analyze_button = InitElement(driver, ".//div[@class='front__landing-text']//button");
        }

        private IWebElement Search_field;
        private IWebElement Analyze_button;

        public HomePage InputSearchURL(string URL)
        {
            Input(Search_field, URL);
            return this;
        }

        public WebsiteScorePage ClickOnAnalyzeButton()
        {
            Click(Analyze_button);
            return new WebsiteScorePage(GetDriver());
        }
    }
}
