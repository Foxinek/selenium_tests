﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace seleniumTests.Pages.Projects.Foxin
{
    //class related to all elements and functions to homepage - http://www.foxin.cz
    [Serializable]
    class HomePage:BasePage
    {
        public HomePage(IWebDriver driver)
        {
            Title_label = InitElement(driver, ".//*[@title='Foxin']");
            Registration_link = InitElement(driver, ".//h4[text()='Recent Posts']//following::a[text()='Registration']");
        }
        private IWebElement Title_label;
        private IWebElement Registration_link;
        
        public RegistrationPage clickOnRegistrationLink()
        {
            Click(Registration_link);
            return new RegistrationPage(GetDriver());
        }

        public string getTitle()
        {
            return GetText(Title_label);
        }
    }
}
