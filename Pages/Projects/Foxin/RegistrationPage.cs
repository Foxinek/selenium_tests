﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Pages.Projects.Foxin
{
    //class related to all elements and functions to registration page http://foxin.cz/uncategorized/registration/
    [Serializable]
    class RegistrationPage :BasePage
    {
        public RegistrationPage(IWebDriver driver)
        {
            Username_label = InitElement(driver, ".//label[@for='user_login']");
            Username_textField = InitElement(driver, ".//*[@id='user_login']");
            Password_textField = InitElement(driver, ".//*[@id='user_pass']");
            Email_textField = InitElement(driver, ".//*[@id='user_email']");
            Confirm_button = InitElement(driver, ".//button[@class='btn button ur-submit-button']");
            Password_label = InitElement(driver, ".//*[@id='user_pass_field']/label");
            Email_label = InitElement(driver, ".//*[@id='user_email_field']/label");
        }

        private IWebElement Username_label;
        private IWebElement Username_textField;
        private IWebElement Password_textField;
        private IWebElement Email_textField;
        private IWebElement Confirm_button;
        private IWebElement Message_text;
        private IWebElement UsernameMessage_text;
        private IWebElement PasswordMessage_text;
        private IWebElement EmailMessage_text;
        private IWebElement Password_label;
        private IWebElement Email_label;

        public RegistrationPage InputUsername(string username)
        {
            Input(Username_textField, username);
            return this;
        }

        public RegistrationPage InputPassword(string password)
        {
            Input(Password_textField, password);
            return this;
        }

        public RegistrationPage InputEmail(string email)
        {
            Input(Email_textField, email);
            return this;
        }

        public RegistrationPage ClickOnConfirmButton()
        {
            Click(Confirm_button);
            return this;
        }

        public string GetMessage()
        {
            Message_text = InitElement(GetDriver(), ".//*[@id='ur-submit-message-node']/ul/li");
            return GetText(Message_text);
        }

        public string GetUsernameErrorMessage()
        {
            UsernameMessage_text = InitElement(GetDriver(), ".//*[@id='user_login-error']");
            return GetText(UsernameMessage_text);
        }

        public string GetPasswordErrorMessage()
        {
            PasswordMessage_text = InitElement(GetDriver(), ".//*[@id='user_pass-error']");
            return GetText(PasswordMessage_text);
        }

        public string GetEmailErrorMessage()
        {
            EmailMessage_text = InitElement(GetDriver(), ".//*[@id='user_email-error']");
            return GetText(EmailMessage_text);
        }

        public string GetUsernameLabel()
        {
            return GetText(Username_label);
        }

        public string GetPasswordLabel()
        {
            return GetText(Password_label);
        }

        public string GetEmailLabel()
        {
            return GetText(Email_label);
        }

    }
}
