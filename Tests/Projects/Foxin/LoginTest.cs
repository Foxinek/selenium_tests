﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Projects.Foxin
{
    //in case tests are run in quick succession please note that tests might fail due to server 503 error
    [TestFixture]
    [Parallelizable]
    class LoginTest
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL("http://foxin.cz/wp-login.php");
        }

        [Test]
        public void loginUsernameTest()
        {          
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string title = loginPage
                .InputUsername("testuser1")
                .InputPassword("testpassword1")
                .ClickOnLoginButton()
                .GetProfileTitle();
            Assert.AreEqual(title, "Profile");
        }

        [Test]
        public void loginEmailTest()
        {
            LoginPage loginPage = new LoginPage(basePage.GetDriver());
            string title = loginPage
                .InputUsername("testuser@testpassword.cz")
                .InputPassword("testpassword1")
                .ClickOnLoginButton()
                .GetProfileTitle();
            Assert.AreEqual(title, "Profile");
        }

        [OneTimeTearDown]
        public void closeBrowser()
        {
            basePage.CloseDriver();
        }
    }
}
