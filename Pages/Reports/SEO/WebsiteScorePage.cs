﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Pages.Reports.SEO
{
    [Serializable]
    class WebsiteScorePage:BasePage
    {
        public WebsiteScorePage(IWebDriver driver)
        {
            CriticalErrors_label = InitElement(driver, ".//app-data-cell[@title='Critical Errors']/div[@class='crawling-data-cell__value']");
            Warnings_label = InitElement(driver, ".//app-data-cell[@title='Warnings']/div[@class='crawling-data-cell__value']");
        }

        private IWebElement CriticalErrors_label;
        private IWebElement Warnings_label;

        public string GetCriticalErrors()
        {
            return GetText(CriticalErrors_label);
        }

        public string GetWarnings()
        {
            return GetText(Warnings_label);
        }
    }
}
