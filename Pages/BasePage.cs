﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using seleniumTests.Helpers;
using Serilog;
using System;

namespace seleniumTests.Pages
{
    //this class is used as base to all other child classes and contains general methods for setting up driver
    //and to implement basic selenium functions
    [Serializable]
    class BasePage
    {
        protected IWebDriver wd;
        protected Driver driver;
        protected HTMLReport htmlReport;

        public BasePage()
        {
        }

        public BasePage(IWebDriver wd)
        {
            this.wd = wd;
        }

        public IWebElement InitElement(IWebDriver driver, string xPath)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 15));
                wait.Until(condition =>
                {
                    try
                    {
                        IWebElement elementToBeDisplayed = driver.FindElement(By.XPath(xPath));
                        return elementToBeDisplayed.Displayed;
                    }
                    catch (StaleElementReferenceException)
                    {
                        return false;
                    }
                    catch (NoSuchElementException)
                    {
                        return false;
                    }
                });
                return driver.FindElement(By.XPath(xPath));
            }
            catch (WebDriverTimeoutException)
            {
                Log.Logger.Error("Couldn't find element in given time");
                CloseDriver();
                return null;
            }
            catch (Exception e)
            {
                Log.Logger.Error(e.StackTrace);
                CloseDriver();
                throw (e);
            }
        }

        public void SetDriver()
        {
            driver = new Driver();
            htmlReport = new HTMLReport();
            driver.CreateDriver(ConfigReader.GetEnvironmentConfiguration()["Browser"].ToLower());
            wd = driver.WebDriver;
            wd.Manage().Window.Maximize();
            htmlReport.InitializeHTMLReporting();
            Logger.InitializeSerilog();
        }

        public void SetURL()
        {
            Log.Debug(ConfigReader.GetProjectConfiguration()["URL"]);
            string url = ConfigReader.GetProjectConfiguration()["URL"];
            GetDriver().Url = url;
        }

        public void SetURL(string url)
        {
            GetDriver().Url = url;
        }

        public string GetReportURL(string report)
        {
            return ConfigReader.GetEnvironmentConfiguration()[report];
        }
        public IWebDriver GetDriver()
        {
            return wd;
        }

        public void WaitForPageToLoad(IWebDriver wd, IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(wd, new TimeSpan(0, 0, 10));
            wait.Until(condition =>
            {
                try
                {
                    return element.Displayed;
                }
                catch (StaleElementReferenceException)
                {
                    return false;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            });
            System.Threading.Thread.Sleep(100);
        }

        public void Input(IWebElement element, string message)
        {
            element.SendKeys(message);
        }
        public void Click(IWebElement element)
        {
            element.Click();
        }

        public string GetText(IWebElement element)
        {
            //WaitForPageToLoad(GetDriver(), element);
            return element.Text;
        }

        public void CloseDriver()
        {
            htmlReport.GenerateHTMLReport(Screenshots.MakeScreenshot(wd));
            wd = null;
            driver.DeleteDriver();
        }

        public string GenerateUserName()
        {
            return "user" + DateTime.Now.ToString(@"MMddhmmtt");
        }

        public string GeneratePassword()
        {
            return "password" + DateTime.Now.ToString(@"MMddhmmtt");
        }

        public string GenerateEmail()
        {
            return "email@" + DateTime.Now.ToString(@"MMddhmmtt") + "a.cz";
        }

    }

}
