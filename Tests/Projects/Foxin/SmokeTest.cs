﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Projects.Foxin
{
    [TestFixture]
    [Parallelizable]
    class SmokeTest
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL();
        }

        [Test]
        public void smokeTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            Assert.AreEqual(homePage.getTitle(), "FOXIN");                
        }

        [OneTimeTearDown]
        public void closeBrowser()
        {
            basePage.CloseDriver();
        }

    }
}
