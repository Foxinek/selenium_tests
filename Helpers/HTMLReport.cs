﻿using System;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using AventStack.ExtentReports;
using System.IO;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System.Threading;
using seleniumTests.Pages;
using seleniumTests.Helpers;

namespace seleniumTests.Helpers
{
    class HTMLReport
    {
        public ExtentReports extentReport;
        public ExtentTest extentTest;
        public void InitializeHTMLReporting()
        {
            try
            {
                extentReport = new ExtentReports();
                var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug", "");
                DirectoryInfo di = Directory.CreateDirectory(dir + "\\Test_Reports");
                var htmlReporter = new ExtentHtmlReporter(dir + "\\Test_Reports" + "\\Test_Report" + ".html");
                extentReport.AddSystemInfo("Project", ConfigReader.GetEnvironmentConfiguration()["Project"]);
                extentReport.AddSystemInfo("Browser", ConfigReader.GetEnvironmentConfiguration()["Browser"].ToUpper());
                extentReport.AttachReporter(htmlReporter);

                extentTest = extentReport.CreateTest(TestContext.CurrentContext.Test.Name);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public void GenerateHTMLReport(string screenshotPath)
        {
            try
            {
                var stacktrace = "" + TestContext.CurrentContext.Result.StackTrace + "";
                switch (TestContext.CurrentContext.Result.Outcome.Status)
                {
                    case TestStatus.Failed:
                        extentTest.Log(Status.Fail, "Test ended with " + Status.Fail + " – " + stacktrace + " " + TestContext.CurrentContext.Result.Message);
                        //extentTest.Log(Status.Fail, "Screenshot " + extentTest.AddScreenCaptureFromPath(screenshotPath));
                        break;
                    case TestStatus.Skipped:
                        extentTest.Log(Status.Skip, "Test ended with " + Status.Skip);
                        break;
                    default:
                        extentTest.Log(Status.Pass, "Test ended with " + Status.Pass);
                        break;
                }
                extentReport.Flush();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
