﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Projects.Foxin
{
    [TestFixture]
    [Parallelizable]
    class RegistrationFieldsTest
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL();
        }

        [Test]
        public void registrationLabelsValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            RegistrationPage registrationPage = homePage
                .clickOnRegistrationLink();
            Assert.AreEqual(registrationPage.GetUsernameLabel(), "Username *");
            Assert.AreEqual(registrationPage.GetPasswordLabel(), "User Password *");
            Assert.AreEqual(registrationPage.GetEmailLabel(), "User Email *");
        }

        [Test]
        public void registrationNullFieldsValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            RegistrationPage registrationPage = homePage
                .clickOnRegistrationLink()
                .InputUsername("")
                .InputPassword("")
                .InputEmail("")
                .ClickOnConfirmButton();
            Assert.AreEqual(registrationPage.GetUsernameErrorMessage(), "This field is required.");
            Assert.AreEqual(registrationPage.GetPasswordErrorMessage(), "This field is required.");
            Assert.AreEqual(registrationPage.GetEmailErrorMessage(), "This field is required.");
        }

        [Test]
        public void userRegisteredValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername("testuser1")
                .InputPassword(basePage.GeneratePassword())
                .InputEmail(basePage.GenerateEmail())
                .ClickOnConfirmButton()
                .GetMessage();
            Assert.AreEqual(message, "Username already exists.");
        }

        [Test]
        public void emailRegisetredValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername(basePage.GenerateUserName())
                .InputPassword(basePage.GeneratePassword())
                .InputEmail("ahoj@ahoj.cz")
                .ClickOnConfirmButton()
                .GetMessage();
            Assert.AreEqual(message, "Email already exists.");
        }

        [Test]
        public void registrationWithoutEmailValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername(basePage.GenerateUserName())
                .InputPassword(basePage.GeneratePassword())
                .InputEmail("")
                .ClickOnConfirmButton()
                .GetEmailErrorMessage();
            Assert.AreEqual(message, "This field is required.");
        }

        [Test]
        public void registrationWithoutUsernameValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername("")
                .InputPassword(basePage.GeneratePassword())
                .InputEmail(basePage.GenerateEmail())
                .ClickOnConfirmButton()
                .GetUsernameErrorMessage();
            Assert.AreEqual(message, "This field is required.");
        }

        [Test]
        public void registrationWithoutPasswordValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername(basePage.GenerateUserName())
                .InputPassword("")
                .InputEmail(basePage.GenerateEmail())
                .ClickOnConfirmButton()
                .GetPasswordErrorMessage();
            Assert.AreEqual(message, "This field is required.");
        }

        [Test]
        public void registrationWithWeakPasswordValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername("pwtest")
                .InputPassword("pwtest")
                .InputEmail(basePage.GenerateEmail())
                .ClickOnConfirmButton()
                .GetMessage();
            Assert.IsTrue(message.Contains("Weak Password!"));
        }

        [Test]
        public void emailSpecialCharactersValidationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername(basePage.GenerateUserName())
                .InputPassword(basePage.GeneratePassword())
                .InputEmail("#@$#@$#@$#@")
                .ClickOnConfirmButton()
                .GetEmailErrorMessage();
            Assert.AreEqual(message, "Please enter a valid email address.");
        }

        [OneTimeTearDown]
        public void closeBrowser()
        {
            basePage.CloseDriver();
        }
    }
}
