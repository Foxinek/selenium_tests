﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Pages.Projects.Foxin
{
    //class related to all elements and functions to login page - http://foxin.cz/wp-login.php
    [Serializable]
    class LoginPage :BasePage
    {

        public LoginPage(IWebDriver driver)
        {
            Login_button = InitElement(driver, ".//*[@id='wp-submit']");
            Username_textField = InitElement(driver, ".//*[@id='user_login']");
            Password_textField = InitElement(driver, ".//*[@id='user_pass']");
            Username_label = InitElement(driver, ".//*[@for='user_login']");
            Password_label = InitElement(driver, ".//*[@for='user_pass']");
        }

        private IWebElement Login_button;
        private IWebElement Username_textField;
        private IWebElement Password_textField;
        private IWebElement Message_text;
        private IWebElement Username_label;
        private IWebElement Password_label;

        public LoginPage InputUsername(string username)
        {
            Input(Username_textField, username);
            return this;
        }

        public LoginPage InputPassword(string password)
        {
            Input(Password_textField, password);
            return this;
        }

        public ProfilePage ClickOnLoginButton()
        {
            Click(Login_button);
            return new ProfilePage(GetDriver());
        }

        public LoginPage ClickOnLoginButtonToValidate()
        {
            Click(Login_button);
            return this;
        }

        public string GetMessage()
        {
            Message_text = InitElement(GetDriver(), ".//*[@id='login_error']");
            return GetText(Message_text);
        }

        public string GetUsernameLabel()
        {
            return GetText(Username_label);
        }

        public string GetPasswordLabel()
        {
            return GetText(Password_label);
        }

    }
}
