﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using Serilog;

namespace seleniumTests.Helpers
{
    class Driver
    {
        public IWebDriver WebDriver { get; set; }

        public void CreateDriver(string browser)
        {
            try
            {
                switch (browser)
                {
                    case "chrome":
                        ChromeOptions chromeOptions = new ChromeOptions();
                        chromeOptions.AddArgument("no-sandbox");
                        chromeOptions.AddArgument("proxy-server='direct://'");
                        chromeOptions.AddArgument("proxy-bypass-list=*");

                        WebDriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, chromeOptions);

                        Log.Debug("will use chrome");
                        break;

                    case "firefox":
                        WebDriver = new FirefoxDriver(AppDomain.CurrentDomain.BaseDirectory);
                        //ThreadLocal<IWebDriver> firefox = new ThreadLocal<IWebDriver>(() =>
                        //{
                        //    return new FirefoxDriver(AppDomain.CurrentDomain.BaseDirectory);
                        //});
                        //driver = firefox.Value;

                        Log.Debug("will use firefox");
                        break;

                    case "ie":
                        InternetExplorerOptions IEOptions = new InternetExplorerOptions();
                        IEOptions.IgnoreZoomLevel = true;

                        WebDriver = new InternetExplorerDriver(AppDomain.CurrentDomain.BaseDirectory, IEOptions);
                        //ThreadLocal<IWebDriver> ie = new ThreadLocal<IWebDriver>(() =>
                        //{
                        //    return new InternetExplorerDriver(AppDomain.CurrentDomain.BaseDirectory, IEOptions);
                        //});
                        //driver = ie.Value;

                        Log.Debug("will use IE");
                        break;

                    default:
                        ThreadLocal<IWebDriver> defaultBrowser = new ThreadLocal<IWebDriver>(() =>
                        {
                            return new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory);
                        });
                        WebDriver = defaultBrowser.Value;

                        Log.Debug("will use default");
                        break;
                }
            }
            catch(Exception e)
            {
                Log.Logger.Error(e.StackTrace);
                throw (e);
            }
        }
        public void DeleteDriver()
        {
            WebDriver.Close();
            WebDriver.Quit();
        }
    }
}
