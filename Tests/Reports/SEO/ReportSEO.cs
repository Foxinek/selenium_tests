﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Reports.SEO
{
    [TestFixture]
    class ReportSEO
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL(basePage.GetReportURL("SEOURL"));
        }

        [Test]
        public void test()
        {

        }
        [OneTimeTearDown]
        public void teardown()
        {
            basePage.CloseDriver();
        }


    }
}
