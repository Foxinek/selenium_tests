﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Pages.Projects.Foxin
{
    //class related to all elements and functions to profile page of logged in user
    [Serializable]
    class ProfilePage :BasePage
    {
        public ProfilePage(IWebDriver driver)
        {
            Profile_label = InitElement(driver, ".//*[@id='profile-page']/h1");
            HomePage_link = InitElement(driver, ".//*[@id='wp-admin-bar-site-name']/a");
        }

        private IWebElement Profile_label;
        private IWebElement HomePage_link;

        public HomePage ClickOnHomePageLink()
        {
            Click(HomePage_link);
            return new HomePage(GetDriver());
        }

        public string GetProfileTitle()
        {
            return GetText(Profile_label);
        }

    }
}
