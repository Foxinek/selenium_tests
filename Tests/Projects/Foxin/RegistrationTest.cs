﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using seleniumTests.Pages.Projects.Foxin;
using seleniumTests.Pages;

namespace seleniumTests.Tests.Projects.Foxin
{
    [TestFixture]
    [Parallelizable]
    class RegistrationTest
    {
        protected BasePage basePage;
        [OneTimeSetUp]
        public void setUp()
        {
            basePage = new BasePage();
            basePage.SetDriver();
            basePage.SetURL();
        }

        [Test]
        public void registrationTest()
        {
            HomePage homePage = new HomePage(basePage.GetDriver());
            string message = homePage
                .clickOnRegistrationLink()
                .InputUsername(basePage.GenerateUserName())
                .InputPassword(basePage.GeneratePassword())
                .InputEmail(basePage.GenerateEmail())
                .ClickOnConfirmButton()
                .GetMessage();
            Assert.AreEqual(message, "User successfully registered.");         
        }

        [OneTimeTearDown]
        public void closeBrowser()
        {
            basePage.CloseDriver();
        }
    }
}
