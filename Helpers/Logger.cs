﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using Serilog.Sinks.SystemConsole;

namespace seleniumTests.Helpers
{
    class Logger
    {
        public static void InitializeSerilog()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.NUnitOutput()
                .CreateLogger();

            Log.Debug("Starting up");
        }
    }
}
