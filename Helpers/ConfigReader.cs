﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seleniumTests.Helpers
{
    abstract class ConfigReader
    {
        public static NameValueCollection GetEnvironmentConfiguration()
        {
            return (NameValueCollection)ConfigurationManager.GetSection("Environment");
        }

        public static NameValueCollection GetProjectConfiguration()
        {
            return (NameValueCollection)ConfigurationManager.GetSection(GetEnvironmentConfiguration()["Project"]);
        }
    }
}
