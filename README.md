Selenium Tests

- Simple selenium tests for website http://www.foxin.cz to validate registration form and login
- Created using selenium page object model and C#

Getting Started

- In order to clone repository please use https://[YourUserName]@bitbucket.org/Foxinek/selenium_tests.git

Running the code

- After cloning the repository build the solution
- Run all tests

Tests in the code

- SmokeTest: checks whether website is loaded
- LoginTest: logs in with precreated username or email
- LoginFieldsTest: tests few situation where website might be incorrectly designed, eg. logging in without password or validation of field names
- RegistrationTest: register new random user
- RegistrationFieldsTest: tests register form on the website, eg. registration with registered username or with null values

Notes

- Please note that server might throw error 503 when login tests are run in quick succession (in this case just wait few minutes or try to run it with pauses)
- There might be some incorrect or non standard code usages, this might be due to my Java background - apologies

Author

- Lukas Filipec

- filipeclukas@gmail.com